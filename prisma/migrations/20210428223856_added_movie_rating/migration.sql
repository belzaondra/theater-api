-- AlterTable
ALTER TABLE "Movie" ADD COLUMN     "IMDBRating" INTEGER NOT NULL DEFAULT 10,
ADD COLUMN     "CinemaRating" INTEGER NOT NULL DEFAULT 10;
