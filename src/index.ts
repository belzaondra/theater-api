import { PrismaClient } from "@prisma/client";
import express from "express";
var cors = require("cors");

const port = 4000;

const prisma = new PrismaClient();

const main = async () => {
  const app = express();

  app.use(cors());

  app.use(express.json());

  app.get("/", (_, res) => {
    res.send("Hello World!");
  });

  app.get("/movies", async (_, res) => {
    const movies = await prisma.movie.findMany();
    res.json(movies);
  });

  app.post("/movies", async (req, res) => {
    const { name, imgUrl, description, trailerLink } = req.body;
    const movie = await prisma.movie.create({
      data: { name, imgUrl, description, trailerLink },
    });
    res.json(movie);
  });

  app.get("/movies/:id", async (req, res) => {
    const { id } = req.params;
    if (!id || typeof id !== "string" || isNaN(parseInt(id)))
      return res.status(400).send();

    var now = new Date();
    const movie = await prisma.movie.findFirst({
      where: { id: parseInt(id) },
      include: {
        playtimes: {
          select: {
            date: true,
            id: true,
          },
          where: {
            date: {
              gte: now,
            },
          },
        },
      },
    });
    if (!movie) return res.status(404).send("Movie not found");
    return res.json(movie);
  });

  app.get("/movies/:id/:playtime", async (req, res) => {
    const { id, playtime } = req.params;
    if (!id || typeof id !== "string" || isNaN(parseInt(id)))
      return res.status(400).send();

    const movie = await prisma.movie.findFirst({
      where: { id: parseInt(id) },
      include: {
        playtimes: {
          select: {
            date: true,
            id: true,
            bookedSeats: true,
          },
          where: {
            id: parseInt(playtime),
          },
        },
      },
    });
    if (!movie) return res.status(404).send("Movie not found");
    return res.json(movie);
  });

  app.post("/movies/:id/playtimes", async (req, res) => {
    const { id } = req.params;
    if (!id || typeof id !== "string" || isNaN(parseInt(id)))
      res.status(400).send();
    const movie = await prisma.movie.findFirst({
      where: { id: parseInt(id) },
      include: {
        playtimes: true,
      },
    });
    if (!movie) return res.status(404).send("Movie not found");

    const date = new Date(req.body.date);
    if (date instanceof Date && !isNaN(date.getTime())) {
      await prisma.playTime.create({
        data: {
          movieId: parseInt(id),
          date: date,
        },
      });
    }

    return res.send();
  });

  app.post("/bookTickets", async (req, res) => {
    const tickets: {
      row: number;
      seat: number;
      movieId: number;
      playtimeId: number;
    }[] = req.body;

    await prisma.bookedSeat.createMany({
      data: tickets.map((t) => ({
        userId: 1,
        row: t.row,
        seat: t.seat,
        playTimeId: t.playtimeId,
      })),
    });
    res.send();
  });

  app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
  });
};

main()
  .catch((e) => console.error(e))
  .finally(async () => await prisma.$disconnect());
